const express = require('express');
const fs = require('fs');
const path = require('path');

const app = express();
const PORT = 3000;

app.use(express.static('public'));
app.use(express.json());

app.get('/read-file', (req, res) => {
    const filePath = path.join(__dirname, 'data', 'file.txt');
    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            return res.status(500).send('Error reading the file.');
        }
        res.send(data);
    });
});

app.post('/write-file', (req, res) => {
    const filePath = path.join(__dirname, 'data', 'file.txt');
    fs.writeFile(filePath, req.body.content, 'utf8', (err) => {
        if (err) {
            return res.status(500).send('Error writing the file.');
        }
        res.send('File successfully updated.');
    });
});

app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});
